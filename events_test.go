package goevents

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type eventsTestSuite struct {
	suite.Suite
	eventBus *InMemoryEventBus
}

func (s *eventsTestSuite) SetupTest() {
	s.eventBus = NewInMemoryEventBus()
}

func (s *eventsTestSuite) TestShutdown() {
	t := s.Assert()

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	go func() {
		s.eventBus.Run()
	}()

	s.eventBus.Shutdown(ctx)

	t.Equal(1, 1)
}

func (s *eventsTestSuite) TestPubSub() {
	t := s.Assert()

	check := 0
	doStuff := func(arg int) {
		check = arg
	}

	s.eventBus.Subscribe("test", doStuff)

	go func() {
		s.eventBus.Run()
	}()

	s.eventBus.Publish("test", 100)
	s.eventBus.Publish("test2", 100)

	s.eventBus.Shutdown(context.Background())

	t.Equal(100, check)
}

func (s *eventsTestSuite) TestPubSubInOrder() {
	t := s.Assert()

	check := make([]int, 0)
	doStuff := func(arg int) {
		check = append(check, arg)
	}

	doSomething := func(arg int) {
		check = append(check, arg*2)
	}

	s.eventBus.Subscribe("test", doStuff)
	s.eventBus.Subscribe("test", doSomething)

	go func() {
		s.eventBus.Run()
	}()

	s.eventBus.Publish("test", 100)

	s.eventBus.Shutdown(context.Background())

	t.Equal([]int{100, 200}, check)
}

func (s *eventsTestSuite) TestPubSubNotAllArguments() {
	t := s.Assert()

	check := make([]int, 0)
	doStuff := func(arg1 int, arg2 int) {
		check = append(check, arg1, arg2)
	}

	s.eventBus.Subscribe("test", doStuff)

	go func() {
		s.eventBus.Run()
	}()

	s.eventBus.Publish("test", 100, 200, 300)

	s.eventBus.Shutdown(context.Background())

	t.Equal([]int{100, 200}, check)
}

func TestEventsSuite(t *testing.T) {
	suite.Run(t, new(eventsTestSuite))
}
