package goevents

import (
	"context"
	"fmt"
	"reflect"
)

// EventBus defines a common interface that all EventBus implementation must follow
type EventBus interface {
	Subscribe(name string, handler interface{})
	Publish(name string, args ...interface{})
	Run()
	Shutdown(ctx context.Context)
}

type internalEvent struct {
	Name string
	Args []interface{}
}

// NewInMemoryEventBus returns a new InMemoryEventBus instance
func NewInMemoryEventBus() *InMemoryEventBus {
	return &InMemoryEventBus{
		handlerMap:   map[string][]reflect.Value{},
		internalChan: make(chan internalEvent),
		exit:         make(chan int, 1),
	}
}

// InMemoryEventBus implements the EventBus interface using go channel
type InMemoryEventBus struct {
	handlerMap   map[string][]reflect.Value
	internalChan chan internalEvent
	exit         chan int
}

// Subscribe listens to a topic
func (e *InMemoryEventBus) Subscribe(topic string, handler interface{}) {
	currentHandlers, ok := e.handlerMap[topic]
	if !ok {
		currentHandlers = make([]reflect.Value, 0)
	}
	currentHandlers = append(currentHandlers, reflect.ValueOf(handler))
	e.handlerMap[topic] = currentHandlers
}

// Publish sends a new event to the topic
func (e *InMemoryEventBus) Publish(topic string, args ...interface{}) {
	e.internalChan <- internalEvent{
		Name: topic,
		Args: args,
	}
}

// based on https://github.com/asaskevich/EventBus/blob/master/event_bus.go#L198
func (e *InMemoryEventBus) prepareArguments(handler reflect.Value, args ...interface{}) []reflect.Value {
	funcType := handler.Type()
	passedArguments := make([]reflect.Value, len(args))
	for i, v := range args {
		if v == nil {
			passedArguments[i] = reflect.New(funcType.In(i)).Elem()
		} else {
			passedArguments[i] = reflect.ValueOf(v)
		}
	}

	return passedArguments[0:funcType.NumIn()]
}

// Run starts the event bus. Without calling this, the subscribers won't be executed after an event has been published
func (e *InMemoryEventBus) Run() {
	for event := range e.internalChan {
		currentHandlers, ok := e.handlerMap[event.Name]
		if ok {
			for _, handler := range currentHandlers {
				handler.Call(e.prepareArguments(handler, event.Args...))
			}
		}
	}
	e.exit <- 1
}

// Shutdown waits for all subscribers to finish before exiting
// A context can be passed to set timeout/deadline etc.. Using a background context will exit without waiting for the subscribers to finish
func (e *InMemoryEventBus) Shutdown(ctx context.Context) {
	close(e.internalChan)

	select {
	case <-e.exit:
		fmt.Println("exiting normally...")
		return
	case <-ctx.Done():
		fmt.Println("exiting due to context...")
		return
	}
}
